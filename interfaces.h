#ifndef INTERFACES_H
#define INTERFACES_H

struct IReceiver
{
    virtual void Receive(const char* data, unsigned int size) = 0;
};


struct ICallback
{
    virtual void BinaryPacket(const char* data, unsigned int size) = 0;
    virtual void TextPacket(const char* data, unsigned int size) = 0;
};


#endif
