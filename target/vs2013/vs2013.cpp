// vs2013.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../../tests/test.h"



int _tmain(int argc, _TCHAR* argv[])
{
  
  DoUnitTest();
  DoPerformanceTest();
  
  return 0;
}

