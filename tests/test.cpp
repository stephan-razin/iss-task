#include "test.h"
#include <vector>
#include <stdint.h>
#include "..\Receiver.h"
#include <iostream>
#include <chrono>

// Data for tests
struct Packet
{
    std::vector<char> data;
    bool isString;
    
    bool operator == (const Packet &r) const
    {
      return isString == r.isString && data.size() == r.data.size() && std::equal(data.begin(), data.end(), r.data.begin());
    }

    Packet(bool is, const char* d, unsigned len):isString(is), data(d, d+len)
    {
    }

    std::vector<char> toRaw() const
    {
        std::vector<char> r;
        if (isString)
        {
            r.insert(r.end(), data.begin(), data.end());
            const char s[] = "\r\n\r\n";
            r.insert(r.end(), s, s + sizeof(s)-1);
        }
        else
        {
            r.push_back(0x24);
            uint32_t l = data.size();
            r.insert(r.end(), (char*)&l, ((char*)&l) + sizeof(l));
            r.insert(r.end(), data.begin(), data.end());
        }
        return r;
    }
};



static const char NORM_BUF[] = "qwfdeytfcybzqbhuiziubzibguygbuyguiuuibihuiuiybiuyuibyiubyiuyui";
static const char LONG_BUF[] = "\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
IQUWIUYEIQYWEIUYQIWYIYQUWIYEIUYQWIYEIUQWYEIUQYWUIEYUIQWYEIUQYWIEUYOPIQPWOIEPOQIWPOEIOIOPIOPIQWPEOIPOIPOIPOIQWIOEICNCNFJJJJJJJJ81\
YIUYIUYIUYIUYIYIUYIYIUYIUYUQYWIHBLZHIQHBWIZHIHBIHQWYEBQLHZNQWLKJNZLQKJMWJMLQKJWE82389YEHNZLKNQ0ZM[ IUNPBYNMPI2[M[JMUNZHP04R-2098\
"; //4Kb

static const Packet shortStr(true,  "_", 1);
static const Packet shortBin(false,  "_", 1);

static const Packet normalStr(true,  NORM_BUF, sizeof(NORM_BUF));
static const Packet normalBin(false, NORM_BUF, sizeof(NORM_BUF));

static const Packet longStr(true,  LONG_BUF, sizeof(LONG_BUF));
static const Packet longBin(false, LONG_BUF, sizeof(LONG_BUF));

static const Packet emptyStr(true, "", 0);
static const Packet emptyBin(false, "", 0);

// 
struct PacketCollector : public ICallback
{
  virtual void BinaryPacket(const char* data, unsigned int size)
  {
    m_collected.push_back(Packet(false, data, size));
  }
  virtual void TextPacket(const char* data, unsigned int size)
  {
    m_collected.push_back(Packet(true, data, size));
  }

  std::vector<Packet> m_collected;
};

struct DummyCallback : public ICallback
{
  virtual void BinaryPacket(const char* data, unsigned int size)
  {
    volatile int k = size; // some delay
    while (--k);
  }
  virtual void TextPacket(const char* data, unsigned int size)
  {
    volatile int k = size; // some delay
    while (--k);
  }
};




static bool testOnePacket(const Packet &pack)
{
    PacketCollector c;
    Receiver r(&c);
    std::vector<char> raw = pack.toRaw();
    r.Receive(raw.data(), raw.size());

    return c.m_collected.size() == 1 && pack == c.m_collected[0];
}

static bool testOnePacketByByte(const Packet &pack)
{
    PacketCollector c;
    Receiver r(&c);
    std::vector<char> raw = pack.toRaw();
    for(auto i : raw)
        r.Receive(&i, 1);

    return c.m_collected.size() == 1 && pack == c.m_collected[0];
}

static bool testSetOfPackets(std::vector<Packet> packs)
{
  PacketCollector c;
  Receiver r(&c);
  std::vector<char> raw;
  for (auto i : packs)
  {
    auto rawI = i.toRaw();
    raw.insert(raw.end(), rawI.begin(), rawI.end());
  }
  r.Receive(raw.data(), raw.size());

  return packs.size() == c.m_collected.size() && std::equal(packs.begin(), packs.end(), c.m_collected.begin());
}

static bool testSetOfPacketsByByte(std::vector<Packet> packs)
{
  PacketCollector c;
  Receiver r(&c);
  std::vector<char> raw;
  for (auto i : packs)
  {
    auto rawI = i.toRaw();
    raw.insert(raw.end(), rawI.begin(), rawI.end());
  }
  for (auto i : raw)
    r.Receive(&i, 1);

  return packs.size() == c.m_collected.size() && std::equal(packs.begin(), packs.end(), c.m_collected.begin());
}



#define CHECK_ONE(func) std::cout << #func; if(!func){std::cout << "- failed!" << std::endl; return false;} else std::cout << " - ok" << std::endl;

bool DoUnitTest()
{
    CHECK_ONE(testOnePacket(shortStr));
    CHECK_ONE(testOnePacket(shortBin));
    CHECK_ONE(testOnePacket(normalStr));
    CHECK_ONE(testOnePacket(normalBin));
    CHECK_ONE(testOnePacket(longStr));
    CHECK_ONE(testOnePacket(longBin));
    CHECK_ONE(testOnePacket(emptyStr));
    CHECK_ONE(testOnePacket(emptyBin));

    CHECK_ONE(testOnePacketByByte(shortStr));
    CHECK_ONE(testOnePacketByByte(shortBin));
    CHECK_ONE(testOnePacketByByte(normalStr));
    CHECK_ONE(testOnePacketByByte(normalBin));
    CHECK_ONE(testOnePacketByByte(longStr));
    CHECK_ONE(testOnePacketByByte(longBin));
    CHECK_ONE(testOnePacketByByte(emptyStr));
    CHECK_ONE(testOnePacketByByte(emptyBin));


    std::vector<Packet> set1 = { shortStr, longStr, longStr, longStr, normalStr, shortStr};
    std::vector<Packet> set2 = { shortBin, shortBin, normalBin, longBin };
    std::vector<Packet> set3 = { longStr, longBin, shortStr, shortStr, normalStr, normalStr, shortBin, longBin };
    std::vector<Packet> set4 = { longBin, longBin, shortStr, shortStr, normalStr, normalStr, shortBin, longBin, longStr, shortBin };
    std::vector<Packet> set5 = { emptyStr, longBin, longBin, shortStr, shortStr, normalStr, emptyStr, normalStr, shortBin, longBin, longStr, shortBin, emptyStr};
    std::vector<Packet> set6 = { emptyBin, longBin, emptyBin, longBin, shortStr, shortStr, normalStr, emptyStr, normalStr, shortBin, longBin, longStr, shortBin, emptyStr, emptyBin, emptyStr, emptyBin };
    
    CHECK_ONE(testSetOfPackets(set1));
    CHECK_ONE(testSetOfPackets(set2));
    CHECK_ONE(testSetOfPackets(set3));
    CHECK_ONE(testSetOfPackets(set4));
    CHECK_ONE(testSetOfPackets(set5));
    CHECK_ONE(testSetOfPackets(set6));

    CHECK_ONE(testSetOfPacketsByByte(set1));
    CHECK_ONE(testSetOfPacketsByByte(set2));
    CHECK_ONE(testSetOfPacketsByByte(set3));
    CHECK_ONE(testSetOfPacketsByByte(set4));
    CHECK_ONE(testSetOfPacketsByByte(set5));
    CHECK_ONE(testSetOfPacketsByByte(set6));

    return true;
}

void DoPerformanceTest()
{
    //preapre data
    std::vector<char> raw;
    auto addPacket = [&raw](const Packet &pack){
        auto i = pack.toRaw();
        raw.insert(raw.end(), i.begin(), i.end());
    };

#if _DEBUG
    for (int i = 0; i <100; ++i){
        addPacket(i % 2 ? longStr : longBin);
    }
#else
    for (int i = 0; i <10000; ++i){
      addPacket(i % 2 ? longStr : longBin);
    }
#endif

    // send data
    DummyCallback c;
    Receiver r(&c);
    uint32_t transfered = 0;
    auto startTm = std::chrono::system_clock::now();
    for (unsigned long i = 0; i < raw.size(); i+=1024)
    {
        const unsigned long toSend = min(1024, raw.size() - i);
        r.Receive(raw.data() + i, toSend);
        transfered += toSend;
    }
    // calc performance
    double duration = std::chrono::duration<double, std::micro>(std::chrono::system_clock::now() - startTm).count();
    double speed = transfered / duration; 
#if _DEBUG
    std::cout << "It's debug build! Performance MUST be checked on release!!!" << std::endl;
#endif
    std::cout << "transfered = " << transfered << " bytes" << std::endl;
    std::cout << "time = " << duration << " us" << std::endl;
    std::cout << "speed = " << speed * 8.0 << " Mbit/sec" << std::endl;
}



