#ifndef RECEIVER_H
#define RECEIVER_H

#include <stdint.h>
#include <vector>
#include "interfaces.h"

#ifndef min
#define min(a,b)    (((a) < (b)) ? (a) : (b))
#endif


class Receiver : public IReceiver
{
public:
    Receiver(ICallback *cb);
    
    virtual ~Receiver(){}

    virtual void Receive(const char* data, unsigned int size);

private:
        
    ICallback *m_callback;
    std::vector<char> m_dataBuf;
};





#endif
