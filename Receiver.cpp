#include "Receiver.h"

static const char STRING_EOL[4]= {'\r','\n','\r','\n'};
static const char BIN_START_BIT = 0x24;
static const unsigned int BIN_HEADER_SIZE = sizeof(uint32_t) + sizeof(BIN_START_BIT); 

Receiver::Receiver(ICallback *cb): m_callback(cb)
{
    m_dataBuf.reserve(1024);    
}

void Receiver::Receive(const char* data, unsigned int size)
{
    if (!data) return; // TODO: error processing

    for(unsigned int dataProcessed = 0; dataProcessed < size;)
    {
        if(!m_dataBuf.size())
            m_dataBuf.push_back(data[dataProcessed++]);
        else
        {
            if(m_dataBuf[0] != BIN_START_BIT) // string data
            {
                while(dataProcessed < size)
                {
                    m_dataBuf.push_back(data[dataProcessed++]);
                    if(m_dataBuf.size() >= sizeof(STRING_EOL) && std::equal(m_dataBuf.end()-sizeof(STRING_EOL), m_dataBuf.end(), STRING_EOL))
                    {
                        m_callback->TextPacket(m_dataBuf.data(), m_dataBuf.size()-sizeof(STRING_EOL));
                        m_dataBuf.clear();
                        break;
                    }
                }
            }
            else // binary data
            {
                while(dataProcessed < size)
                {
                    if(m_dataBuf.size() < BIN_HEADER_SIZE ) // still no size
                        m_dataBuf.push_back(data[dataProcessed++]);
                    if(m_dataBuf.size() >= BIN_HEADER_SIZE)
                    {
                        uint32_t packSize = 0; memcpy(&packSize, &m_dataBuf[1], sizeof(packSize));
                        const uint32_t toCopy = min(packSize - (m_dataBuf.size() - BIN_HEADER_SIZE), size - dataProcessed);
                        m_dataBuf.insert(m_dataBuf.end(), &data[dataProcessed], &data[dataProcessed+toCopy]);
                        dataProcessed += toCopy;
                        if(m_dataBuf.size() >= packSize+BIN_HEADER_SIZE)
                        {
                            m_callback->BinaryPacket(m_dataBuf.data()+BIN_HEADER_SIZE, m_dataBuf.size()-BIN_HEADER_SIZE);
                            m_dataBuf.clear();
                            break;
                        }
                    }
                }
            }
        }
    }//for

}



